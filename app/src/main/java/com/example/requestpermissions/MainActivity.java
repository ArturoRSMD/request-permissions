package com.example.requestpermissions;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final int PHONE_CALL_CODE = 100;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateVersion();
            }
        });



    }

    public void validateVersion(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            currentVersionPermission();
        } else {
            oldVersionPermission();
        }
    }

    public void currentVersionPermission(){
        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {

        switch (requestCode){
            case PHONE_CALL_CODE:

                String permission = permissions[0];
                int granResult = grantResults[0];

                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    if (granResult == PackageManager.PERMISSION_GRANTED){
                        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel: 5512341234"));
                        startActivity(intentCall);
                    } else{
                        Toast.makeText(this, "Sin permisos", Toast.LENGTH_SHORT).show();
                    }



                }





        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void oldVersionPermission(){
        if (checkPermission(Manifest.permission.CALL_PHONE)){
            // Intent de llamada
            Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel: 5535160336"));
            startActivity(intentCall);

        } else{
            Toast.makeText(this, "No has concedido permisos de cmara para el dispositivo", Toast.LENGTH_SHORT).show();
        }

    }

    public boolean checkPermission(String permission){
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}
